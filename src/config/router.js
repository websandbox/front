import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import Pricing from '../views/Pricing'
import SignIn from '../user/SignIn'
import SignUp from '../user/SignUp'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/pricing',
        name: 'pricing',
        component: Pricing
    },
    {
        path: '/sign-in',
        name: 'signIn',
        component: SignIn
    },
    {
        path: '/sign-up',
        name: 'signUp',
        component: SignUp
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router

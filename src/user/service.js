import fetch from '../config/fetch'

export const checkLogin = async (email, password) => {

    const body = {
        email: email,
        password: password
    };

    return await fetch(process.env.VUE_APP_API_URL + '/login_check', {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
        },
        body: JSON.stringify(body),
    })
        .then((user) => {
            localStorage.setItem('user-token', user.token)
            return user
        })
        .catch(
            (error) => console.log(error))
}

export const registerUser = async (form) => {
    
    const body = {
        email: form.email,
        password: form.password,
        username: form.username
    }

    return await fetch(process.env.VUE_APP_API_URL + '/users', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    })
        .then((data) => data)
        .catch((error) => console.log(error))
}

export const getCurrentUser = async () => {

    const token = localStorage.getItem('user-token')

    const body = {
        token: token
    }

    return await fetch(process.env.VUE_APP_API_URL + '/user_from_token', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(body)
    })
        .then((data) => {
            if (data.isValid) {
                return data.user
            } else {
                return null
            }
        })
        .catch(err => {
            console.log(err)
            if (err.json) {
                return err.json.then(json => {
                    console.log(json)
                })
            }
        })
}
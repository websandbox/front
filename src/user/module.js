import { getCurrentUser } from './service'

const user = {
    token: localStorage.getItem('user-token') || '',
    email: '',
    username: '',
}

const getters = {
    user: state => state.user
}

const mutations = {
    EDIT_USER: (state, user) => {
        state.user.email = user.email;
        state.user.token = user.token;
        state.user.username = user.username;
    },
}

const actions = {
    editUser: (store, user) => {
        store.commit('EDIT_USER', user)
    },
    getUserFromToken: async (store) => {
        const data = await getCurrentUser();
        if (data) {
            store.commit('EDIT_USER', data)
        }
    }
}

export default {
    state: () => ({ user }),
    mutations: mutations,
    actions: actions,
    getters: getters,
}
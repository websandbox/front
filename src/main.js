import Vue from 'vue'
import router from './config/router'
import App from './App.vue'
import VueFormulate from '@braid/vue-formulate'
import './assets/css/formulate.css'

Vue.use(VueFormulate)

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
